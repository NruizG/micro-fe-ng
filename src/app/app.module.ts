import { BrowserModule } from '@angular/platform-browser';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MicroFrontComponent } from './micro-front/micro-front.component';
import { BusinessModule } from './business/business.module';

@NgModule({
  declarations: [
    AppComponent,
    MicroFrontComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BusinessModule
  ],
  entryComponents: [
    AppComponent,
    MicroFrontComponent
  ]
})
export class AppModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void {
    const  { injector } = this;

    const mfView = createCustomElement(MicroFrontComponent, { injector });
    customElements.define('micro-test', mfView);
  }
}
